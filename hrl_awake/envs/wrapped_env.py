import gym


class WrapEnv(gym.Env):

    def __init__(self,env:gym.Env):
        self.env = env
        self.observation_space = self.env.observation_space
        self.action_space = self.env.action_space
        self.action_episode_memory = []
        self.rewards = []
        self.initial_conditions = []
        self.current_episode = -1
        self.current_steps =0


    def step(self, action):
        return_state, return_reward,is_finalized, _ = self.env.step(action)
        self.action_episode_memory[self.current_episode].append(action)
        self.rewards[self.current_episode].append(return_reward)

        self.current_steps += 1
        print(f"return {return_reward}, is_finalized {is_finalized}, current number srteps {self.current_steps}")
        return return_state, return_reward, is_finalized, {}


    def reset(self):
        self.current_episode += 1
        self.current_steps = 0
        self.action_episode_memory.append([])
        self.rewards.append([])

        initial_state = self.env.reset()
        self.initial_conditions.append([initial_state])
        return initial_state


