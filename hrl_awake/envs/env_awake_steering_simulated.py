import math
import random
from enum import Enum
import gym
import numpy as np


from gym import spaces
import gym

class AwakeTrajectorysimENV(gym.Env):

    def __init__(self, twiss,**kwargs):
        self.__version__ = "0.0.1"

        # General variables defining the environment
        self.MAX_TIME = 50
        self.is_finalized = False
        self.current_episode = -1

        self.current_steps = 0

        self.seed()

        self.twiss_bpms = twiss[twiss['keyword'] == 'monitor']
        self.twiss_correctors = twiss[twiss['keyword'] == 'kicker']

        # self.response_scale = 0.5

        self.plane = Plane.horizontal

        self.response = self._calculate_response(self.twiss_bpms, self.twiss_correctors,self.plane)

        self.positions = np.zeros(len(self.twiss_bpms)-1)#remove one BPM
        self.settings = np.zeros(len(self.twiss_correctors)-1)#remove on corrector

        high = np.ones(len(self.settings))
        low = (-1) * high
        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)
        # self.act_lim = self.action_space.high[0]

        high = np.ones(len(self.positions))
        low = (-1) * high
        self.observation_space = spaces.Box(low=low, high=high, dtype=np.float32)


        if 'action_scale' in kwargs:
            self.action_scale = kwargs.get('action_scale')
        else:
            self.action_scale = 3e-4
        if 'state_scale' in kwargs:
            self.state_scale = kwargs.get('state_scale')
        else:
            self.state_scale = 100
        self.kicks_0 = np.zeros(len(self.settings))

        self.threshold = -0.0016*self.state_scale #corresponds to 1.6 mm scaled.


    def recalculate_response(self):
        self.response = self._calculate_response(self.twiss_bpms, self.twiss_correctors, self.plane)


    def step(self, action):

        state, reward = self._take_action(action)

        self.current_steps += 1
        if self.current_steps > self.MAX_TIME:
            self.is_finalized = True

        return_reward = reward * self.state_scale
        return_state = np.array(state * self.state_scale)
        if (return_reward > self.threshold) or (return_reward < 15*self.threshold):
            self.is_finalized = True

        return return_state, return_reward, self.is_finalized, {}

    def step_opt(self, action):
        state, reward = self._take_action(action,is_optimisation=True)
        return_reward = reward * self.state_scale
        return return_reward

    def _take_action(self, action, is_optimisation = False):

        kicks = action * self.action_scale

        state, reward = self._get_state_and_reward(kicks,is_optimisation)

        return state, reward

    def _get_reward(self, trajectory):
        rms = np.sqrt(np.mean(np.square(trajectory)))
        return (rms * (-1.))

    def _get_state_and_reward(self, kicks, is_optimisation):
        rmatrix = self.response

        state = self._calculate_trajectory(rmatrix, self.kicks_0+kicks)

        if(not is_optimisation):
            self.kicks_0 = self.kicks_0+kicks

        reward = self._get_reward(state)
        return state, reward

    def _calculate_response(self, bpmsTwiss, correctorsTwiss,plane):
        bpms = bpmsTwiss.index.values.tolist()
        correctors = correctorsTwiss.index.values.tolist()
        bpms.pop(0)
        correctors.pop(-1)

        rmatrix = np.zeros((len(bpms), len(correctors)))

        for i, bpm in enumerate(bpms):
            for j, corrector in enumerate(correctors):
                if (plane == Plane.horizontal):
                    bpm_beta = bpmsTwiss.betx[bpm]
                    corrector_beta = correctorsTwiss.betx[corrector]
                    bpm_mu = bpmsTwiss.mux[bpm]
                    corrector_mu=correctorsTwiss.mux[corrector]
                else:
                    bpm_beta = bpmsTwiss.bety[bpm]
                    corrector_beta = correctorsTwiss.bety[corrector]
                    bpm_mu = bpmsTwiss.muy[bpm]
                    corrector_mu = correctorsTwiss.muy[corrector]

                if (bpm_mu > corrector_mu):
                    rmatrix[i][j] = math.sqrt(bpm_beta * corrector_beta) * math.sin(
                        (bpm_mu - corrector_mu) * 2. * math.pi)
                else:
                    rmatrix[i][j] = 0.0
        return rmatrix


    def _calculate_trajectory(self, rmatrix, delta_settings):

        delta_settings = np.squeeze(delta_settings)
        return rmatrix.dot(delta_settings)

    def reset(self, **kwargs):

        self.is_finalized = False
        self.current_steps = 0

        self.settings = np.random.uniform(-1., 1., len(self.settings))
        self.kicks_0 = self.settings * self.action_scale

        state = self._calculate_trajectory(self.response, self.kicks_0)

        # Rescale for agent
        return_initial_state = np.array(state * self.state_scale)

        return return_initial_state

    def seed(self, seed=None):
        random.seed(seed)

    def setPlane(self, plane):
        if (plane == Plane.vertical or plane == Plane.horizontal):
            self.plane = plane
        else:
            raise Exception("You need to set plane enum")
        self.response = self._calculate_response(self.twiss_bpms, self.twiss_correctors, self.plane)


class Plane(Enum):
    horizontal = 0
    vertical = 1