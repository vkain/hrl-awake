import matplotlib.pyplot as plt
import numpy as np
from stable_baselines3 import TD3
from stable_baselines3.td3.policies import MlpPolicy
from stable_baselines3.common.noise import NormalActionNoise
from cpymad.madx import Madx

from hrl_awake.envs.env_awake_steering_simulated import AwakeTrajectorysimENV
from hrl_awake.envs.wrapped_env import WrapEnv


OPTIONS = ['ECHO', 'WARN', 'INFO', 'DEBUG', 'TWISS_PRINT']
MADX_OUT = [f'option, -{ele};' for ele in OPTIONS]

madx = Madx()
madx.input('\n'.join(MADX_OUT))
madx.options.echo=False;madx.options.warn=True;

tt43_ini= "envs/electron_design.mad"

madx.call(file=tt43_ini,chdir=True)
madx.use(sequence= "tt43",range= '#s/plasma_merge')


madx.input('initbeta0:beta0,BETX=5,ALFX=0,DX=0,DPX=0,BETY=5,ALFY=0,DY=0.0,DPY=0.0,x=0,px=0,y=0,py=0;')
twiss_cpymad =madx.twiss(beta0='initbeta0').dframe()

env_real = AwakeTrajectorysimENV(twiss_cpymad)

env = WrapEnv(env_real)

